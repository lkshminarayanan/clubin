# Clubin

## How to Install

### Software/Module prerequisites
1. perl
2. apache2
3. Install cgi perl mod in apache.<br/>
   `sudo apt-get install libapache2-mod-perl2`
4. Install CPAN to enable downloading perl modules.<br/>
   `sudo apt-get install cpan`
5. Install the following perl modules using `sudo cpan install *module-name*`<br/>
   * DBI
   * DBI:SQLite
   * CGI
   * JSON
   * Switch

### Pre-installation steps
1. Change apache to run as local user rather than its own user.<br/>
   Do this by changing the User and Group Values to the local user's value in `/etc/apache2/apache.conf`<br/>
   eg. : <br/>
    `User lkshminarayanan
    Group lkshminarayanan`
2. Enable cgi in apache<br/>
   `sudo a2enmod cgi`
3. Configure cgi in apache
   * Include the following line in /etc/apache2/sites-available/000-default.conf within the section <VirtualHost *:80>.<br/>
     `Include conf-available/serve-cgi-bin.conf`<br/>
     (It might be already there commented out. Just uncomment it)
   * Change ScriptAlias' directory to clubin's script directory in `/etc/apache2/conf-available/serve-cgi-bin.conf`.<br/>
     (By default the project copies the scripts into `/usr/lib/cgi-bin/clubin/`)<br/>
     Also change the handler from `/cgi-bin/` to `/clubin-cgi/`.
4. Restart apache server.<br/>
   `sudo service apache2 restart`
   
### Install
1. Install the scripts into their locations by issuing follwing commands<br/>
   `cp web-pages/cgi-scripts/* /usr/lib/cgi-bin/clubin/`<br/>
   `cp web-pages/html/* /var/www/html/clubin/`
   
## Starting Clubin
Start clubin by starting the scheduler.pl in background.

## FAQs

### How to enable clusterj tests?
1. Download the latest MySQL Connector/J in the test machine.
2. Set the path of the jar file to $connector_j_path variable in globals.pl.
