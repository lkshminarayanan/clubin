#!/usr/bin/perl
use warnings ;
use strict ;

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

#trial inputs
#TODO: get inputs from web page
my $branch_name="mysql-5.6-cluster-7.4";
my $job_name="lsreetha-fk";
my $compile_opts="";
my $mtr_opts="";

#make sure the inputs are not empty/null
if(!defined $branch_name || $branch_name eq "" ||
   !defined $job_name || $job_name eq "")
{
  die "Incomplete inputs";
}

#insert the job details into clubin.db
my $job_time=time();
$dbh->do('INSERT INTO jobs (name, branch_name, compile_opts, mtr_opts, timestamp) VALUES (?, ?, ?, ?, ?)',
  undef,
  $job_name, $branch_name, $compile_opts, $mtr_opts, $job_time);

#query the jobs table to get id
my $job_selector = $dbh->prepare('SELECT id
                                  FROM jobs
                                  ORDER BY id DESC
                                  LIMIT 1;');
$job_selector->execute();

#get the id
my @job = $job_selector->fetchrow_array();
my $job_id = $job[0];

#my $job_dir=$workspace."/".$job_name."-".$job_id;
my $job_dir=get_working_dir($job_name,$job_id);

#create the job directory
mkdir($job_dir) or die "$!";

#TODO copy the patch into this folder
