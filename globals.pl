#!/usr/bin/perl
use strict;
use warnings;

#global constants
our $repo="/home/lkshminarayanan/repo/test/mysql";
our $workspace="/home/lkshminarayanan/Projects/Clubin/workspace";
our $connector_j_path="/home/lkshminarayanan/mysql-connector-java-5.1.37-bin.jar";

sub get_working_dir{
  # @params job-name, job-id
  # @result workspace/job_name-id
  return $workspace."/".$_[0]."-".$_[1];
}
