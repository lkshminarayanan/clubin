#!/usr/bin/perl
use warnings ;
use strict ;
use File::Path qw(make_path remove_tree);
use File::MimeInfo;

#import the global constants
require "globals.pl";
our($repo, $connector_j_path);

#import the database handler
require "init-dbh.pl";
our($dbh);

my ($status);

sub exit_job{
  # prints the status, updates the result in the db and exits
  my $exit_code = defined $_[0] ? $_[0] : 1;
  my $error_msg = defined $_[1] ? $_[1] : 'Unknown Error';
  if($exit_code){
    #error in run.
    print "Error : ".$error_msg;
  }
  #TODO also update the result
  $dbh->do('UPDATE jobs SET result = ? WHERE status = 1', undef, ($exit_code)?0:1);
  #mark the current job as done
  $dbh->do('UPDATE jobs SET status = 2 WHERE status = 1');
  #cleanup temp dir
  remove_tree("/tmp/clubin", {keep_root => 1, safe => 1});
  #exit 1 for error and 0 for success
  exit $exit_code;
}

#query the jobs table for the first pending job
my $job_selector = $dbh->prepare('SELECT id, name, branch_name,
                                         patch, compile_opts,
                                         mtr_opts, timestamp
                                  FROM jobs
                                  WHERE status == 1
                                  ORDER BY id
                                  LIMIT 1;')
       or exit_job(1, "Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);
$job_selector->execute()
       or exit_job(1, "Couldn't execute 'SELECT id'. Error: ".$dbh->errstr);

#try read the first job
my @job = $job_selector->fetchrow_array();

print "Executing job : @job\n";

my $job_id         =$job[0];
my $job_name       =$job[1];
my $branch_name    =$job[2];
my $patch_name     =$job[3];
my $compile_opts   =$job[4];
my $mtr_opts       =$job[5];
my $job_time       =$job[6];

my $job_dir=get_working_dir($job_name,$job_id);

#switch to job_dir
chdir($job_dir) or exit_job(1, "$!");

#redirect stdout/stderr to logfiles
open(LOG, '>', 'run.log') or exit_job(1, "Unable to open log file");
*STDOUT = *LOG;
*STDERR = *LOG;

sub execute_command{
  #params command, message to display on failure.
  my $cmd = $_[0];
  my $err_msg = $_[1];
  print "Executing '$cmd'\n";
  $status=`$cmd 2>&1`;
  if($? != 0)
  {
    exit_job(1, $err_msg."\nError code : $?\nOutput from command :\n$status");
  }
}

#fetch all the remote branches
execute_command("git -C $repo fetch origin", "Git fetch failed");

#checkout the branch in root repo
execute_command("git -C $repo checkout $branch_name", "Checking out the branch failed.");

#to handle forced update
execute_command("git -C $repo reset --hard origin/$branch_name", "Error handling forced update.");

#pull the latest code once
execute_command("git -C $repo pull --rebase", "git pull failed.");

#now clone the specified branch into job_dir
execute_command("git clone $repo --branch $branch_name --single-branch $branch_name", "cloning the branch failed.");

#apply patch to local repo if submitted
if($patch_name){
  my $mime_type = mimetype($job_dir."/".$patch_name);
  if($mime_type eq "message/rfc822"){
    #patch mail
    execute_command("git -C $branch_name am $job_dir/$patch_name", "Error in applying the submitted patch mail.");
  } else {
    #normal patch
    execute_command("git -C $branch_name apply $job_dir/$patch_name", "Error in applying the submitted patch.");
  }
}

#start building
mkdir("build") or exit_job(1, "$!");
chdir("build") or exit_job(1, "$!");
my $def_compile_opts="--debug";
$compile_opts=(!defined $compile_opts || $compile_opts eq "")?$def_compile_opts:$compile_opts;

print "Configuring source tree..\n";
print "Running : ../$branch_name/storage/ndb/compile-cluster $compile_opts --just-configure > $job_dir/cmake.log\n";
system("../$branch_name/storage/ndb/compile-cluster $compile_opts --just-configure > $job_dir/cmake.log 2>&1") and
  exit_job(1, "cmake failed.");
print "Done.\n";

print "Running Make\nmake -j12 > $job_dir/make.log\n";
system("make -j12 > $job_dir/make.log 2>&1") and
  exit_job(1, "make failed.");
print "Done.\n";

#prepare for mtr'ing
chdir("mysql-test") or exit_job(1, "$!");
#set MTR_CLASSPATH
$ENV{MTR_CLASSPATH}=$connector_j_path;
#make a tmp dir for mtr
my $tmpdir="/tmp/clubin/d".$job_id;
make_path($tmpdir) or exit_job(1, "$!");
print "Running MTR..\n";
print "./mtr $mtr_opts --parallel=auto --force --tmpdir=$tmpdir > $job_dir/mtr.log\n";
system("./mtr $mtr_opts --parallel=auto --force --tmpdir=$tmpdir > $job_dir/mtr.log 2>&1") and
  exit_job(1, "MTR Failed.");
print "Done.\n";

#success
exit_job(0);
