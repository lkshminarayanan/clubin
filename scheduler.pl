#!/usr/bin/perl
use warnings ;
use strict ;

#import the database handler
require "globals.pl";
our($workspace);

#redirect output to log/err files
open(STDOUT, '>', $workspace.'/scheduler.log') or die "Can't open log";
open(STDERR, '>', $workspace.'/scheduler.err') or die "Can't open log";

#import the database handler
require "init-dbh.pl";
our($dbh);

#create the jobs table if not existing
#status : 0 - new/pending; 1 - running; 2 - done;
#result : 0 - fail; 1 - pass; valid only after status=2;
my $jobs_schema = <<'END_SCHEMA';
CREATE TABLE IF NOT EXISTS jobs (
  id           INTEGER      PRIMARY KEY AUTOINCREMENT,
  name         VARCHAR(100) NOT NULL,
  branch_name  VARCHAR(100) NOT NULL,
  patch        BOOLEAN          NULL DEFAULT "",
  compile_opts VARCHAR          NULL DEFAULT "",
  mtr_opts     VARCHAR          NULL DEFAULT "",
  timestamp    DATETIME     NOT NULL,
  status       INTEGER      NOT NULL DEFAULT 0,
  result       INTEGER      NOT NULL DEFAULT 0
)
END_SCHEMA
$dbh->do($jobs_schema);

#disable buffering in print
select((select(STDOUT), $|=1)[0]);

while(1){
  #query the jobs table for the first pending job
  my $job_selector = $dbh->prepare('SELECT id, name
                                    FROM jobs
                                    WHERE status == 0
                                    ORDER BY id
                                    LIMIT 1;');
  $job_selector->execute();

  #try read the first job
  my @job = $job_selector->fetchrow_array();

  if(!@job){
    #sleep since no new job.
    sleep 1;
    next;
  }

  #mark the current job as running
  $dbh->do('UPDATE jobs SET status = 1 WHERE id = ?', undef, $job[0]);

  #Execute job
  print "Picked up job : $job[0].$job[1]\n";
  system("./run-job.pl");
}
