#!/usr/bin/perl
use warnings;
use strict;

use CGI qw(:standard);
use File::Path 'rmtree';
use JSON;

sub return_error{
  # returns json error and exits
  my $json->{"error"} = $_[0];
  $json->{"result"} = 0;
  print to_json($json);
  exit 1;
}

#print Content-Type
print header('application/json');

#import the cgi global to get project dir
require "cgi-global.pl";
our ($project_dir);

#move into the project folder since this will be inside cgi-bin
chdir($project_dir) or return_error("Invalid project dir config");

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

my $q = CGI->new;

my $job_id = $q->param("id");

if(!$job_id){
  return_error("Invalid job id");
}

#check if job exist
my $job_selector = $dbh->prepare('SELECT id, name, timestamp, status
                                  FROM jobs
                                  WHERE id = ?')
                      or return_error("Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);

$job_selector->execute($job_id)
  or return_error("Couldn't execute 'SELECT id'. Error : ".$job_selector->errstr);

my @job = $job_selector->fetchrow_array();
if(!@job){
  #job doesn't exist
  return_error("Invalid job id");
}

#if the job is running, don't delete
if($job[3] == 1){
  return_error("Can't delete running job");
}

#get working dir from these details
my $job_dir = get_working_dir($job[1], $job[0]);

#job exists.. remove from db first
$dbh->do('DELETE FROM jobs WHERE id = ?', undef, $job_id)
  or return_error("Unable to delete job. Error: ".$dbh->errstr);

#remove the working dir
rmtree($job_dir, 0, 0);

my $ret_json->{"result"} = 1;
print to_json($ret_json);
