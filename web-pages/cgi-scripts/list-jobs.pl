#!/usr/bin/perl
use warnings;
use strict;

use CGI qw(:standard);
use JSON;

sub return_error{
  # returns json error and exits
  my $json->{"error"} = $_[0];
  print to_json($json);
  exit 1;
}

#print Content-Type
print header('application/json');

#import the cgi global to get project dir
require "cgi-global.pl";
our ($project_dir);

#move into the project folder since this will be inside cgi-bin
chdir($project_dir) or return_error("Invalid project dir config");

#redirect error log
open(STDERR, '>', 'workspace/list-jobs.err') or return_error("Invalid error-log config");

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

my $q = CGI->new;

my $offset        = $q->param("offset");
my $jobs_per_page = 10;

if(!$offset){
  $offset=0;
}

#query the latest jobs
my $job_selector = $dbh->prepare('SELECT * 
                                  FROM jobs
                                  ORDER BY id DESC
                                  LIMIT ?
                                  OFFSET ?;')
                      or return_error("Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);

$job_selector->execute($jobs_per_page, $offset)
  or return_error("Couldn't execute 'SELECT id'. Error : ".$job_selector->errstr);

#loop and get the jobs in json
my $job_count = 0;
my @job_list = ();
while (my @job = $job_selector->fetchrow_array() ){
  $job_count++;
  my $job_json = {};
  $job_json->{"id"}           = $job[0];
  $job_json->{"name"}         = $job[1];
  $job_json->{"branch_name"}  = $job[2];
  $job_json->{"patch_name"}   = $job[3];
  $job_json->{"compile_opts"} = $job[4];
  $job_json->{"mtr_opts"}     = $job[5];
  $job_json->{"timestamp"}    = localtime($job[6]);
  $job_json->{"status"}       = $job[7];
  $job_json->{"result"}       = $job[8];
  if($job[7] > 0){
    #move into working dir
    my $job_dir=get_working_dir($job[1],$job[0]);
    chdir($job_dir) or return_error("Unable to fetch log files.");
    
    #check the status of the build
    $job_json->{"cmake"}  = 1 if -f "cmake.log";
    $job_json->{"make"}   = 1 if -f "make.log";
    $job_json->{"mtr"}    = 1 if -f "mtr.log";
  }
  push @job_list, $job_json;
}
if($job_count == 0){
  return_error("Nothing to show");
}

my $ret_json->{"count"} = $job_count;
$ret_json->{"jobs"} = \@job_list;

#query total jobs
$job_selector = $dbh->prepare('SELECT COUNT(*)
                               FROM jobs')
                   or return_error("Couldn't prepare 'SELECT COUNT'. Error: ".$dbh->errstr);

$job_selector->execute()
  or return_error("Couldn't execute 'SELECT COUNT'. Error : ".$job_selector->errstr);

my @job = $job_selector->fetchrow_array();
my $total_num_jobs = $job[0];
if($offset != 0){
  #there are newer jobs to show
  $ret_json->{"has_newer_jobs"} = 1;
}
if ($total_num_jobs != 0 && $total_num_jobs > ($offset + $job_count)){
  #there are older jobs to show
  $ret_json->{"has_older_jobs"} = 1;
}

print to_json($ret_json);
