#!/usr/bin/perl
use warnings;
use strict;

use CGI qw(:standard);
use File::Copy;
use JSON;

sub return_error{
  # returns json error and exits
  my $json->{"error"} = $_[0];
  $json->{"result"} = 0;
  print to_json($json);
  exit 1;
}

#print Content-Type
print header('application/json');

#import the cgi global to get project dir
require "cgi-global.pl";
our ($project_dir);

#move into the project folder since this will be inside cgi-bin
chdir($project_dir) or return_error("Invalid project dir config");

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

my $q = CGI->new;

my $old_job_id = $q->param("id");

if(!$old_job_id){
  return_error("Invalid job id");
}

#check if job exist
my $job_selector = $dbh->prepare('SELECT name, branch_name, patch, compile_opts, mtr_opts 
                                  FROM jobs
                                  WHERE id = ?')
                      or return_error("Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);

$job_selector->execute($old_job_id)
  or return_error("Couldn't execute 'SELECT id'. Error : ".$job_selector->errstr);

my @old_job = $job_selector->fetchrow_array();
if(!@old_job){
  #job doesn't exist
  return_error("Invalid job id");
}

#old job exists, with details
my ($job_name, $branch_name, $compile_opts, $mtr_opts, $job_time, $patch_name);
$job_name     = $old_job[0];
$branch_name  = $old_job[1];
$patch_name   = $old_job[2];
$compile_opts = $old_job[3];
$mtr_opts     = $old_job[4];
$job_time     = time();

#start inserting the details as new in clubin.db
$dbh->do('INSERT INTO jobs (name, branch_name, patch, compile_opts, mtr_opts, timestamp) VALUES (?, ?, ?, ?, ?, ?)',
  undef,
  $job_name, $branch_name, $patch_name, $compile_opts, $mtr_opts, $job_time)
      or return_error("Unable to push job into db. Error: ".$dbh->errstr);

#query the jobs table to get id
$job_selector = $dbh->prepare('SELECT id
                               FROM jobs
                               ORDER BY id DESC
                               LIMIT 1;')
                   or return_error("Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);

$job_selector->execute()
  or return_error("Couldn't execute 'SELECT id'. Error : ".$job_selector->errstr);

#get the new id
my @job = $job_selector->fetchrow_array();
my $job_id = $job[0];

#get working dir
my $job_dir=get_working_dir($job_name,$job_id);

#create the job directory
mkdir($job_dir) or return_error("$!");
chdir($job_dir) or return_error("$!");

#handle patch if present
if($patch_name){
  #get working dir of old job
  my $old_job_dir = get_working_dir($job_name,$old_job_id);
  #copy patch from old dir to new dir
  copy($old_job_dir."/".$patch_name,$job_dir."/".$patch_name)
    or return_error "Copying patch failed: $!";
}

my $job_json->{"id"}        = $job_id;
$job_json->{"name"}         = $job_name;
$job_json->{"branch_name"}  = $branch_name;
$job_json->{"patch_name"}   = $patch_name;
$job_json->{"compile_opts"} = $compile_opts;
$job_json->{"mtr_opts"}     = $mtr_opts;
$job_json->{"timestamp"}    = localtime($job_time);
$job_json->{"status"}       = 0;
$job_json->{"result"}       = 0;

my $ret_json->{"result"} = 1;
$ret_json->{"job"} = $job_json;

print to_json($ret_json);
