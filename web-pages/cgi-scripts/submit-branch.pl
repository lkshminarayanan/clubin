#!/usr/bin/perl
use warnings;
use strict;

use CGI qw(:standard);
use JSON;

sub return_error{
  # returns json error and exits
  my $json->{"result"} = 0;
  $json->{"error"} = $_[0];
  print to_json($json);
  exit 1;
}

#print Content-Type
print header('application/json');

#import the cgi global to get project dir
require "cgi-global.pl";
our ($project_dir);

my ($job_name, $branch_name, $compile_opts, $mtr_opts, $job_time, $patch_name);

#move into the project folder since this will be inside cgi-bin
chdir($project_dir) or return_error("Invalid project dir config");

#redirect error log
open(STDERR, '>', 'workspace/submit-branch.err') or return_error("Invalid error-log config");

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

my $q = CGI->new;

$job_name     = $q->param("name");
$branch_name  = $q->param("branch-name");
$compile_opts = $q->param("compile-opts");
$mtr_opts     = $q->param("mtr-opts");
$patch_name   = $q->param("job-patch");
$job_time=time();

if(!$job_name || !$branch_name){
  return_error("Invalid name/branch-name");
}

#change any spaces in name to `-`
$job_name=~s/ /-/g;

my $patch_handle;
if($patch_name){
  #user uploaded a patch
  #strip the remote path and keep the filename
  $patch_name=~m/^.*(\\|\/)(.*)/;
  #get file handle
  $patch_handle = $q->upload("job-patch");
  #check if its fully uploaded
  if (!$patch_handle && $q->cgi_error) {
    return_error($q->cgi_error);
  }
  #check the patch file type
  my $type = $q->uploadInfo($patch_handle)->{'Content-Type'};
  my @allowed_types = ("text/plain", "text/x-patch",
                       "text/x-diff", "application/x-patch",
                       "application/x-diff", "application/x-extension-eml");
  my @match = grep(/^$type/i, @allowed_types);
  if (@match != 1){
   return_error("Unsupported patch file type - ".$type);
  }
}

#insert the job details into clubin.db
$dbh->do('INSERT INTO jobs (name, branch_name, patch, compile_opts, mtr_opts, timestamp) VALUES (?, ?, ?, ?, ?, ?)',
  undef,
  $job_name, $branch_name, $patch_name, $compile_opts, $mtr_opts, $job_time)
      or return_error("Unable to push job into db. Error: ".$dbh->errstr);

#query the jobs table to get id
my $job_selector = $dbh->prepare('SELECT id
                                  FROM jobs
                                  ORDER BY id DESC
                                  LIMIT 1;')
                   or return_error("Couldn't prepare 'SELECT id'. Error: ".$dbh->errstr);

$job_selector->execute()
  or return_error("Couldn't execute 'SELECT id'. Error : ".$job_selector->errstr);

#get the id
my @job = $job_selector->fetchrow_array();
my $job_id = $job[0];

#my $job_dir=$workspace."/".$job_name."-".$job_id;
my $job_dir=get_working_dir($job_name,$job_id);

#create the job directory
mkdir($job_dir) or return_error("$!");
chdir($job_dir) or return_error("$!");

#handle patch if present
if($patch_name){
  #copy patch into this folder
  open(my $local_patch_file, '>', $patch_name) or return_error("Can't open local patch file $patch_name : $!");
  while( <$patch_handle> ){
    print $local_patch_file $_;
  }
  close($patch_handle);
  close($local_patch_file);
}

#script successfully run
my $json->{"result"} = 1;
print to_json($json);
