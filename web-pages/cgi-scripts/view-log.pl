#!/usr/bin/perl
use warnings;
use strict;
use Switch;

use CGI qw(:standard);

sub return_invalid_request{
  print "\nInvalid request.\n";
  exit 1;
}

#HTTP Header
print "Content-type: text/plain\n";

#import the cgi global to get project dir
require "cgi-global.pl";
our ($project_dir);

#move into the project folder since this will be inside cgi-bin
chdir($project_dir) or return_invalid_request();

#import the global constants
require "globals.pl";

#import the database handler
require "init-dbh.pl";
our($dbh);

my $q = CGI->new;

#get params
my $job_id   = $q->param("id");
#$job_file values
#1:patch 2:run.log 3:cmake.log 4:make.log 5:mtr.log
my $job_file = $q->param("file");

#query the jobs table to get job name
my $job_selector = $dbh->prepare('SELECT name, patch
                                  FROM jobs
                                  WHERE id == ?;');

$job_selector->execute($job_id);

#try read the first job
my @job = $job_selector->fetchrow_array();

if(!@job){
  return_invalid_request();
}

my $job_name   = $job[0];
my $patch_name = $job[1];

if(!$job_id || !$job_name || !$job_file){
  return_invalid_request();
}

my $file_name;
my @available_logs = ("run", "cmake", "make", "mtr");
switch($job_file){
  case 1 {
    if(!$patch_name){
      return_invalid_request();
    }
    $file_name=$patch_name;
  }
  case [2..5] {
    $file_name=$available_logs[$job_file-2].".log";
  }
  else {
    return_invalid_request();
  }
}
print "Content-Disposition: inline; filename=".$file_name."\n\n";

my $job_dir=get_working_dir($job_name,$job_id);
chdir($job_dir) or return_invalid_request();

# Actual File Content
my $buffer;
open(FILE, $file_name ) or return_invalid_request();
while(read(FILE, $buffer, 100) )
{
  print("$buffer");
}
