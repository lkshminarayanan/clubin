//var page = 0;
var jobs_per_page = 10;
var offset = 0;//page * jobs_per_page;
var home_page = "cerberus";
var cgi_handler = "cerberus-cgi";

var show_error = function(msg){
  $("#msg").html('<div style="width:40%" class="alert alert-danger" role="alert">'+msg+'</div>');
};

var register_handlers = function(id){
  $("#delete-job-"+id).click(function(e){
    e.preventDefault();
    if(!confirm("Do you want to delete Job-"+id+"?.\nAction is not reversible!")){
      return;
    }
    $("#msg").html("");
    $.post("/" + cgi_handler + "/delete-job.pl",{"id":id}, function(data){
      if(data.result === 1){
        //success.
        $("#job-"+id).hide();
      } else {
        show_error('<strong>Error!</strong> '+data.error);
      }
    }, "json").fail(function(){
     show_error('Failed to complete delete request. Try again!</div>');
    });
  });
  $("#repeat-job-"+id).click(function(e){
    e.preventDefault();
    $("#msg").html("");
    $.post("/" + cgi_handler + "/repeat-job.pl",{"id":id}, function(data){
      if(data.result === 1){
        //success.
        var job = data.job;
        var row = fill_job_row(job);
        $("#job-list").prepend(row);
        register_handlers(job.id);
      } else {
        show_error('<strong>Error!</strong> '+data.error);
      }
    }, "json").fail(function(){
     show_error('Failed to complete repeat request. Try again!</div>');
    });
  });
};

var fill_job_row = function(job){
  var row = "<tr id='job-"+job.id+"' class='";
  if(job.status === 2){
    if(job.result === 1)
      row += "success";
    else
      row += "danger";
  } else if (job.status === 1){
    row += "info";
  }
  row += "'>";
  row += "<td>" + job.name + "<br/>";
  row += job.branch_name;
  if(job.patch_name !== "") row += "<br/> Applied patch : <a href='/" + cgi_handler + "/view-log.pl?file=1&id="+job.id+"'>" + job.patch_name + "</a>";
  if(job.compile_opts !== "") row += "<br/>Compile opts : " + job.compile_opts;
  if(job.mtr_opts !== "") row += "<br/>MTR opts : " + job.mtr_opts;
  row += "<br/>" + job.timestamp;
  if(job.status !== 1)
    row += '<br/><a href="#" id="delete-job-'+job.id+'"><span class="glyphicon glyphicon-trash" title="Delete Job" style="color:crimson"></span></a>';
  if(job.status === 2)
    row += '&nbsp;&nbsp;<a href="#" id="repeat-job-'+job.id+'"><span class="glyphicon glyphicon-repeat" title="Repeat Job" style="color:#577492"></span></a>';
  row += "</td><td>";
  if(job.status > 0)           row += "<a href='/" + cgi_handler + "/view-log.pl?file=2&id="+job.id+"'>run.log</a><br/>";
  if(job.cmake  !== undefined) row += "<a href='/" + cgi_handler + "/view-log.pl?file=3&id="+job.id+"'>cmake.log</a><br/>";
  if(job.make   !== undefined) row += "<a href='/" + cgi_handler + "/view-log.pl?file=4&id="+job.id+"'>make.log</a><br/>";
  if(job.mtr    !== undefined) row += "<a href='/" + cgi_handler + "/view-log.pl?file=5&id="+job.id+"'>mtr.log</a><br/>";
  row += "</td>";
  row += "</tr>";
  return row;
}

var load_page = function(){
  $("#job-table").hide();
  $("#job-list").html("");
  $("#pager, #pager-older, #pager-newer").hide();
  $.post("/" + cgi_handler + "/list-jobs.pl", {
    "offset": offset
  }, function(data) {
    //check for error
    if(data.error !== undefined){
     show_error('<strong>Error!</strong>'+data.error);
    }
    var jobs = data.jobs;
    var count = data.count;
    for (var i=0;i<count;i++){
      var job = jobs[i];
      var row = fill_job_row(job);
      $("#job-list").append(row);
      register_handlers(job.id);
    }
    $("#job-table").show();
    if (data.has_newer_jobs !== undefined) $("#pager-newer").show();
    if (data.has_older_jobs !== undefined) $("#pager-older").show();
    if (data.has_older_jobs !== undefined || data.has_newer_jobs !== undefined) $("#pager").show();
  }, "json").fail(function(){
    //failed to do ajax
     show_error('Failed to fetch job list. Try again!</div>');
  });
}

$(document).ready(function(){
  load_page();
  //affix side bar
  $("#sidepane").width($("#sidepane").width());
  $("#sidepane").affix({
    offset: $("#sidepane").position() - 10
  });
  //register pagination
  $('#pager').on('click', 'a', function(e) {
    e.preventDefault();
    console.log(this);
    if(this.id === "get-newer-job")
      offset -= jobs_per_page;
    else
      offset += jobs_per_page;
    load_page();
  });
});
