$(document).ready(function(){
  var home_page = "cerberus";
  var cgi_handler = "cerberus-cgi";
  $("#submit-branch").submit(function(e){
    e.preventDefault();
    //set the compile opt option
    var compile_opts= "";
    var enabledOpts = $("#compile-opts-checkbox .active");
    for(var i = 0; i < enabledOpts.length; i++){
      var opt = $(enabledOpts[i]).attr("title");
      compile_opts += " " + opt;
      if(opt === "--vanilla"){
        //also make mtr a vanilla run
        $("#mtr-opts").val($("#mtr-opts").val() + " --skip-ndbcluster");
      }
    }
    compile_opts += " " + $("#other-compile-opts").val();
    $("#compile-opts").val(compile_opts);
    console.log(compile_opts);
    //make sure the job is not a plain build
    var bname = $("#branch-name").val();
    var pname = $("#job-patch").val();
    if((bname === "mysql-5.5-cluster-7.2" ||
        bname === "mysql-5.6-cluster-7.3" ||
        bname === "mysql-5.6-cluster-7.4" ||
        bname === "mysql-5.7-cluster-7.5" )&&
       (pname === undefined || pname === "")&&
       (compile_opts === undefined || compile_opts === "")){
      //ask the user if this is intended
      if(!confirm("You have requested a test on a plain branch without any patch.\n"+
                  "Are you sure you want to submit?\n"+
                  "(This can probably be checked in PB2!)")){
        return;
      }
    }
    $("#result").html("");
    var formData = new FormData(this);
    $.ajaxSetup({
      processData: false,
      contentType: false
    });
    $.post("/" + cgi_handler + "/submit-branch.pl", formData, function(data) {
      var alert_class="";
      var msg="";
      if(data.result){
        //success
        alert_class="success";
        msg="<strong>Success!</strong> Your branch has been submitted. <br/>View results <a href='/" + home_page + "'>here</a>.";
        //$("#submit-branch :input").prop("disabled", true);
        $("#submit-branch").hide();
      } else {
        alert_class="danger";
        msg="<strong>Error!</strong> "+data.error+".";
      }
      var alert='<div style="width:40%" class="alert alert-'+alert_class+'" role="alert">'+msg+'</div>';
      $("#result").html(alert);
    }, "json").fail(function(){
       //failed to do ajax
       $("#result").html('<div style="width:40%" class="alert alert-danger" role="alert">Failed to submit. Retry again.</div>');
    });
  });

  $("#advanced-switch a").click(function(e){
    e.preventDefault();
    $(".advanced").toggle();
    $(".advanced-radio").toggle();
  });

  var register_radio_btns = function(btn_input_id){
    var radio_selector = "#" + btn_input_id + "-radio button";
    $(radio_selector).click(function(e){
      e.preventDefault();
      $(radio_selector).removeClass("active");
      $(this).addClass("active").blur();
      $("#"+btn_input_id).val($(this).attr("title"));
    });
  }
  register_radio_btns("branch-name");
  register_radio_btns("mtr-opts");

  var register_checkbox_btns = function(checkbox_selector){
    $(checkbox_selector).click(function(e){
      e.preventDefault();
      $(e.target).toggleClass("active").blur();
    });
  }
  register_checkbox_btns("#compile-opts-checkbox");

});
